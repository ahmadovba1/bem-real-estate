package com.bem.estate.repo;

import com.bem.estate.domain.listing.sale.house.SaleIndependentHouse;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SaleIndependentHouseRepo extends JpaRepository<SaleIndependentHouse, Long > {

}
