package com.bem.estate.repo;

import com.bem.estate.domain.listing.rent.house.RentApartmentHouse;
import com.bem.estate.domain.listing.sale.house.SaleApartmentHouse;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RentApartmentHouseRepo extends JpaRepository<RentApartmentHouse, Long> {



}
