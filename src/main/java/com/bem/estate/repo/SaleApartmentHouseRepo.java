package com.bem.estate.repo;

import com.bem.estate.domain.listing.ApartmentHouse;
import com.bem.estate.domain.listing.SaleDetails;
import com.bem.estate.domain.listing.sale.house.SaleApartmentHouse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


public interface SaleApartmentHouseRepo extends JpaRepository<SaleApartmentHouse, Long > {

}
