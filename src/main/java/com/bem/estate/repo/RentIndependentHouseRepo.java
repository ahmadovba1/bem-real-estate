package com.bem.estate.repo;

import com.bem.estate.domain.listing.rent.house.RentApartmentHouse;
import com.bem.estate.domain.listing.rent.house.RentIndependentHouse;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RentIndependentHouseRepo extends JpaRepository<RentIndependentHouse, Long> {



}
