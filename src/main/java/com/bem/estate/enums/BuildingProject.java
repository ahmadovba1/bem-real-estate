package com.bem.estate.enums;

public enum BuildingProject {
    STALIN, KHRUSCHEV, FRENCH, MINSK, KIEV, LENINGRAD, EXPERIMENTAL;
}
