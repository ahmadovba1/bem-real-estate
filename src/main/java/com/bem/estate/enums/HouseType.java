package com.bem.estate.enums;

public enum HouseType {
    INDEPENDENT_HOUSE, BUILDING_HOUSE;
}
