package com.bem.estate.enums;

public enum RepairSituation {
    ZERO, NEEDED, NORMAL, FULL
}
