package com.bem.estate.util;

import com.bem.estate.domain.address.City;
import com.bem.estate.domain.address.District;
import com.bem.estate.domain.address.EconomicRegion;
import com.bem.estate.domain.address.Province;
import com.bem.estate.repo.address.CityRepo;
import com.bem.estate.repo.address.DistrictRepo;
import com.bem.estate.repo.address.EconomicRegionRepo;
import com.bem.estate.repo.address.ProvinceRepo;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@RequiredArgsConstructor
@Component
public class CSVService {
    private final EconomicRegionRepo economicRegionRepo;
    private final CityRepo cityRepo;
    private final DistrictRepo districtRepo;
    private final ProvinceRepo provinceRepo;

    public void createEconomicRegions() throws FileNotFoundException {
        List<ImportDto> list = importFRomCsv("src/economic_region.csv");
        list.stream()
                .forEach(a -> {
                    economicRegionRepo.save(
                            EconomicRegion.builder()
                                    .id(a.getOtherTableId())
                                    .name(a.getName())
                                    .build());
                });

    }

    public void createCities() throws FileNotFoundException {
        List<ImportDto> list = importFRomCsv("src/city.csv");
        list.stream()
                .forEach(a -> {
                    cityRepo.save(
                            City.builder()
                                    .name(a.getName())
                                    .economicRegion(economicRegionRepo.findById(a.getOtherTableId()).orElseThrow(NullPointerException::new))
                                    .build());
                });

    }

    public void createDistricts() throws FileNotFoundException {
        List<ImportDto> list = importFRomCsv("src/district.csv");
        list.stream()
                .forEach(a -> {
                    districtRepo.save(
                            District.builder()
                                    .name(a.getName())
                                    .city(cityRepo.findById(a.getOtherTableId()).orElseThrow(NullPointerException::new))
                                    .build());
                });
    }

    public void createProvinces() throws FileNotFoundException {
        List<ImportDto> list = importFRomCsv("src/province.csv");
        list.stream()
                .forEach(a -> {
                    provinceRepo.save(
                            Province.builder()
                                    .name(a.getName())
                                    .district(districtRepo.findById(a.getOtherTableId()).orElseThrow(NullPointerException::new))
                                    .build());
                });
    }

    private List<ImportDto> importFRomCsv(String fileName) throws FileNotFoundException {
        return new CsvToBeanBuilder(new FileReader(fileName))
                .withType(ImportDto.class)
                .build()
                .parse();
    }
}
