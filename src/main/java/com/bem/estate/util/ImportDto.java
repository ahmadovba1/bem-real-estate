package com.bem.estate.util;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ImportDto {

    private String name;
    private Long OtherTableId;

}
