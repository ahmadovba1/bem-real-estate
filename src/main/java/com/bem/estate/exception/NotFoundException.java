package com.bem.estate.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GeneralException {

    public NotFoundException(Enum<?> message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
