package com.bem.estate.exception;

import org.springframework.http.HttpStatus;

public class GeneralException extends RuntimeException{
    private final String message;
    private final HttpStatus httpStatus;

    public GeneralException(Enum<?> message, HttpStatus httpStatus) {
        super(message.name());
        this.message = message.name();
        this.httpStatus = httpStatus;
    }
}
