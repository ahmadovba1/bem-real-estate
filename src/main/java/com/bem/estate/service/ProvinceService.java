package com.bem.estate.service;

import com.bem.estate.dto.address.ProvinceDto;
import org.springframework.data.domain.Page;

public interface ProvinceService {
    Page<ProvinceDto> getProvincesByDistrictId(Long districtId);
    ProvinceDto getProvinceById(Long provinceId);
}
