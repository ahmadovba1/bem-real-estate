package com.bem.estate.service;

import com.bem.estate.domain.address.Location;
import com.bem.estate.dto.address.LocationDto;
import lombok.RequiredArgsConstructor;

 public interface LocationService {
     void setLocation(Location location);

}
