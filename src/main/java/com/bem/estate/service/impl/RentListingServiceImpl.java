package com.bem.estate.service.impl;

import com.bem.estate.domain.listing.rent.house.RentApartmentHouse;
import com.bem.estate.domain.listing.rent.house.RentIndependentHouse;
import com.bem.estate.dto.listing.request.RentApartmentHouseDto;
import com.bem.estate.dto.listing.request.RentIndependentHouseDto;
import com.bem.estate.dto.mapper.RentApartmentHouseMapper;
import com.bem.estate.dto.mapper.RentIndependentHouseMapper;
import com.bem.estate.repo.RentApartmentHouseRepo;
import com.bem.estate.repo.RentIndependentHouseRepo;
import com.bem.estate.service.CommonListingDetailsService;
import com.bem.estate.service.RentListingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RentListingServiceImpl implements RentListingService {
    private final RentApartmentHouseRepo rentApartmentHouseRepo;
    private final RentIndependentHouseRepo rentIndependentHouseRepo;
    private final RentApartmentHouseMapper rentApartmentHouseMapper;
    private final RentIndependentHouseMapper rentIndependentHouseMapper;
    private final CommonListingDetailsService commonListingDetailsService;

    @Override
    public void listRentApartmentHouse(RentApartmentHouseDto rentApartmentHouseDto) {
        RentApartmentHouse rentApartmentHouse = rentApartmentHouseMapper.toEntity(rentApartmentHouseDto);
        commonListingDetailsService.setCommonListingDetails(rentApartmentHouse.getApartmentHouse().getHouse().getBuilding().getCommonListingDetails());
        rentApartmentHouseRepo.save(rentApartmentHouse);
    }

    @Override
    public void listRentIndependentHouse(RentIndependentHouseDto rentIndependentHouseDto) {
        RentIndependentHouse rentIndependentHouse = rentIndependentHouseMapper.toEntity(rentIndependentHouseDto);
        commonListingDetailsService.setCommonListingDetails(rentIndependentHouse.getIndependentHouse().getHouse().getBuilding().getCommonListingDetails());
        rentIndependentHouseRepo.save(rentIndependentHouse);

    }

    @Override
    public List<RentApartmentHouseDto> findAllRentApartmentHouse() {
        return rentApartmentHouseMapper.toDtoList(rentApartmentHouseRepo.findAll());
    }

    @Override
    public List<RentIndependentHouseDto> findAllRentIndependentHouse() {
        return rentIndependentHouseMapper.toDtoList(rentIndependentHouseRepo.findAll());
    }
}
