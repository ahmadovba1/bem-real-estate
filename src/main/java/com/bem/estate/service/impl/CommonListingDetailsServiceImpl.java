package com.bem.estate.service.impl;

import com.bem.estate.domain.address.City;
import com.bem.estate.domain.listing.CommonListingDetails;
import com.bem.estate.dto.address.CityDto;
import com.bem.estate.dto.mapper.location.CityMapper;
import com.bem.estate.enums.Status;
import com.bem.estate.repo.address.CityRepo;
import com.bem.estate.service.CommonListingDetailsService;
import com.bem.estate.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommonListingDetailsServiceImpl implements CommonListingDetailsService {
    private final LocationService locationService;

    @Override
    public void setCommonListingDetails(CommonListingDetails commonListingDetails) {
        locationService.setLocation(commonListingDetails.getLocation());
        commonListingDetails.setStatus(Status.ACTIVE);
    }
}

