package com.bem.estate.service.impl;

import com.bem.estate.domain.listing.sale.house.SaleApartmentHouse;
import com.bem.estate.domain.listing.sale.house.SaleIndependentHouse;
import com.bem.estate.dto.listing.request.SaleApartmentHouseDto;
import com.bem.estate.dto.listing.request.SaleIndependentHouseDto;
import com.bem.estate.dto.mapper.SaleApartmentHouseMapper;
import com.bem.estate.dto.mapper.SaleIndependentHouseMapper;
import com.bem.estate.repo.SaleApartmentHouseRepo;
import com.bem.estate.repo.SaleIndependentHouseRepo;
import com.bem.estate.service.CommonListingDetailsService;
import com.bem.estate.service.SaleListingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SaleListingServiceImpl implements SaleListingService {
    private final SaleApartmentHouseRepo saleApartmentHouseRepo;
    private final SaleIndependentHouseRepo saleIndependentHouseRepo;
    private final SaleApartmentHouseMapper saleApartmentHouseMapper;
    private final SaleIndependentHouseMapper saleIndependentHouseMapper;
    private final CommonListingDetailsService commonListingDetailsService;


    @Override
    public void listSaleApartmentHouse(SaleApartmentHouseDto saleApartmentHouseDto) {
        SaleApartmentHouse saleApartmentHouse = saleApartmentHouseMapper.toEntity(saleApartmentHouseDto);
        commonListingDetailsService.setCommonListingDetails(saleApartmentHouse.getApartmentHouse().getHouse().getBuilding().getCommonListingDetails());
        saleApartmentHouseRepo.save(saleApartmentHouse);
    }

    @Override
    public void listSaleIndependentHouse(SaleIndependentHouseDto saleIndependentHouseDto) {
        SaleIndependentHouse saleIndependentHouse = saleIndependentHouseMapper.toEntity(saleIndependentHouseDto);
        commonListingDetailsService.setCommonListingDetails(saleIndependentHouse.getIndependentHouse().getHouse().getBuilding().getCommonListingDetails());
        saleIndependentHouseRepo.save(saleIndependentHouse);
    }

    @Override
    public List<SaleIndependentHouseDto> findAllSaleIndependentHouse() {

        return   saleIndependentHouseMapper.toDtoList(saleIndependentHouseRepo.findAll());
    }

    @Override
    public List<SaleApartmentHouseDto> findAllSaleApartmentHouse() {
        return   saleApartmentHouseMapper.toDtoList(saleApartmentHouseRepo.findAll());
    }
}
