package com.bem.estate.service.impl;

import com.bem.estate.domain.address.Location;
import com.bem.estate.dto.address.LocationDto;
import com.bem.estate.service.CityService;
import com.bem.estate.service.DistrictService;
import com.bem.estate.service.LocationService;
import com.bem.estate.service.ProvinceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {
    private final CityService cityService;
    private final DistrictService districtService;
    private final ProvinceService provinceService;

    @Override
    public void setLocation(Location location) {
        long cityId = cityService.findById(location.getCityId()).getId();
        location.setCityId(cityId);
        location.setDistrictId(districtService.getDistrictById(location.getDistrictId()).getId());
        location.setProvinceId(provinceService.getProvinceById(location.getProvinceId()).getId());

    }


}
