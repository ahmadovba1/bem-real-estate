package com.bem.estate.service;

import com.bem.estate.dto.listing.request.RentApartmentHouseDto;
import com.bem.estate.dto.listing.request.RentIndependentHouseDto;

import java.util.List;

public interface RentListingService {

    void listRentApartmentHouse(RentApartmentHouseDto rentApartmentHouseDto);
    void listRentIndependentHouse(RentIndependentHouseDto rentIndependentHouseDto);


    List<RentApartmentHouseDto> findAllRentApartmentHouse();

    List<RentIndependentHouseDto> findAllRentIndependentHouse();
}
