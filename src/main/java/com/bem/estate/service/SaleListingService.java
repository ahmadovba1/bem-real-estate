package com.bem.estate.service;

import com.bem.estate.domain.listing.sale.house.SaleApartmentHouse;
import com.bem.estate.dto.listing.request.SaleApartmentHouseDto;
import com.bem.estate.dto.listing.request.SaleIndependentHouseDto;

import java.util.List;

public interface SaleListingService {

    void listSaleApartmentHouse(SaleApartmentHouseDto saleApartmentHouseDto);
    void listSaleIndependentHouse(SaleIndependentHouseDto saleIndependentHouseDto);


    List<SaleApartmentHouseDto> findAllSaleApartmentHouse();

    List<SaleIndependentHouseDto> findAllSaleIndependentHouse();
}
