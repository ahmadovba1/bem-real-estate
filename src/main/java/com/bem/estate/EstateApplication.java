package com.bem.estate;

import com.bem.estate.util.CSVService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class EstateApplication implements CommandLineRunner {
    private final CSVService csvService;

    public static void main(String[] args) {
        SpringApplication.run(EstateApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        csvService.createEconomicRegions();
      csvService.createCities();
      csvService.createDistricts();
        csvService.createProvinces();
    }


}
