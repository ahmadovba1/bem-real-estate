package com.bem.estate.dto.mapper.location;

import com.bem.estate.domain.address.Province;
import com.bem.estate.dto.address.ProvinceDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProvinceMapper {

    ProvinceMapper INSTANCE = Mappers.getMapper(ProvinceMapper.class);


    ProvinceDto ProvinceToProvinceDto(Province Province);


    Province ProvinceDtoToProvince(ProvinceDto ProvinceDto);


}
