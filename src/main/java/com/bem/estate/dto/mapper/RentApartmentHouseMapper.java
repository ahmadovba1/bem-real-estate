package com.bem.estate.dto.mapper;

import com.bem.estate.domain.listing.rent.house.RentApartmentHouse;
import com.bem.estate.domain.listing.rent.house.RentIndependentHouse;
import com.bem.estate.dto.listing.request.RentApartmentHouseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RentApartmentHouseMapper {

    RentApartmentHouseMapper INSTANCE = Mappers.getMapper(RentApartmentHouseMapper.class);

    RentApartmentHouse toEntity(RentApartmentHouseDto dto);
    List<RentApartmentHouseDto> toDtoList(List<RentApartmentHouse> entityList);


}
