package com.bem.estate.dto.mapper;

import com.bem.estate.domain.listing.sale.house.SaleApartmentHouse;
import com.bem.estate.dto.listing.request.SaleApartmentHouseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SaleApartmentHouseMapper {

    SaleApartmentHouseMapper INSTANCE = Mappers.getMapper(SaleApartmentHouseMapper.class);

    SaleApartmentHouse toEntity(SaleApartmentHouseDto dto);

    List<SaleApartmentHouseDto> toDtoList(List<SaleApartmentHouse> all);
}
