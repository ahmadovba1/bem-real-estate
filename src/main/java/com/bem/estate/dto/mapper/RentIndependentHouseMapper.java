package com.bem.estate.dto.mapper;

import com.bem.estate.domain.listing.rent.house.RentIndependentHouse;
import com.bem.estate.dto.listing.request.RentIndependentHouseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RentIndependentHouseMapper {

    RentIndependentHouseMapper INSTANCE = Mappers.getMapper(RentIndependentHouseMapper.class);

    RentIndependentHouse toEntity(RentIndependentHouseDto dto);
    List<RentIndependentHouseDto> toDtoList(List<RentIndependentHouse> entityList);

}
