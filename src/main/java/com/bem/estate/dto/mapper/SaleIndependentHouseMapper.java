package com.bem.estate.dto.mapper;

import com.bem.estate.domain.listing.sale.house.SaleIndependentHouse;
import com.bem.estate.dto.listing.request.SaleIndependentHouseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SaleIndependentHouseMapper {

    SaleIndependentHouseMapper INSTANCE = Mappers.getMapper(SaleIndependentHouseMapper.class);

    SaleIndependentHouse toEntity(SaleIndependentHouseDto dto);
    List<SaleIndependentHouseDto> toDtoList(List<SaleIndependentHouse> entityList);

}
