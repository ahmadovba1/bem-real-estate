package com.bem.estate.dto.listing;

import com.bem.estate.domain.listing.CommonListingDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Setter
@Getter
public class LandDto {

    private Double landSquareOfficial;
    private Double landSquareUnOfficial;
}
