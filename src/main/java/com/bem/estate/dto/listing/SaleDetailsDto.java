package com.bem.estate.dto.listing;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;


@Setter
@Getter
public class SaleDetailsDto {


    private Boolean livingNow;
    private Boolean mortgageAvailable;
    private Boolean inMortgageNow;
    private Boolean barter;
}
