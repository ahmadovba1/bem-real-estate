package com.bem.estate.dto.listing;

import com.bem.estate.domain.listing.Building;
import com.bem.estate.enums.HouseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Setter
@Getter
public class HouseDto {

    private BuildingDto building;
    @Enumerated(EnumType.STRING)
    private HouseType houseType;
}
