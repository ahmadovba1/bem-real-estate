package com.bem.estate.dto.listing;

import com.bem.estate.domain.listing.CommonListingDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Setter
@Getter
public class BuildingDto {

    private CommonListingDetailsDto commonListingDetails;
    private Double BuildingSquareUnofficial;
    private Double BuildingSquareOfficial;
    private Integer deliveryYear;
    private Integer buildingFloorSize;
    private Double utilityPaymentM2;
    private Integer roomNumber;
    private Boolean furnished;
    private String remainedFurniture;
    private Boolean centralHeating;
    private Boolean continuousWater;
    private Boolean gas;
    private Boolean internet;
    private Boolean cableTv;
    private Boolean telephone;
    private Boolean combiBoiler;
    private Boolean hotFloor;
    private String viewDirection;
    private Integer floor;
    private Integer balconyNumber;
    private Double ceilingHeight;
    private Boolean storage;
    private String repairSituation;
}
//todo building utility- hemde heyet evlerine aid olacaq. onu duzelt