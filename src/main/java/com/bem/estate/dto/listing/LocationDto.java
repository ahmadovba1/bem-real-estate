package com.bem.estate.dto.listing;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Setter
@Getter
public class LocationDto {

    @ApiModelProperty(example = "1")
    Long cityId;
    @ApiModelProperty(example = "1")
    Long districtId;
    @ApiModelProperty(example = "1")
    Long provinceId;
    Long villageId;
    Double lat_t;
    Double long_t;
    String addressDescription;

//    @ManyToMany
//    List<NearPoint> nearPoints;
}
