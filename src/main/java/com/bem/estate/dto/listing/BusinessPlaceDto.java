package com.bem.estate.dto.listing;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class BusinessPlaceDto {

    private BuildingDto building;
}
