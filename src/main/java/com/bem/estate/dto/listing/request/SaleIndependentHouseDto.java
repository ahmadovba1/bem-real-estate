package com.bem.estate.dto.listing.request;

import com.bem.estate.dto.listing.IndependentHouseDto;
import com.bem.estate.dto.listing.SaleDetailsDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SaleIndependentHouseDto {

    private SaleDetailsDto saleDetails;
    private IndependentHouseDto independentHouse;
}
