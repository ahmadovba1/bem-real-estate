package com.bem.estate.dto.listing;

import com.bem.estate.domain.listing.House;
import com.bem.estate.enums.ApartmentType;
import com.bem.estate.enums.BuildingProject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Setter
@Getter
public class ApartmentHouseDto {

    private HouseDto house;
    private Integer buildingConstructionYear;

    @Enumerated(EnumType.STRING)
    private ApartmentType apartmentType;


    @Enumerated(EnumType.STRING)
    private BuildingProject buildingProject;

}
