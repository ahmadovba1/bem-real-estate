package com.bem.estate.dto.listing;

import com.bem.estate.domain.address.Location;
import com.bem.estate.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Setter
@Getter
public class CommonListingDetailsDto {

    private Double price;
    private Boolean officialRegistrationDocument;
    private Boolean workWithAgent;
    private LocationDto location;
    private String listingDescription;

}
