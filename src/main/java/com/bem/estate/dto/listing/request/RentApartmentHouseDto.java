package com.bem.estate.dto.listing.request;

import com.bem.estate.dto.listing.ApartmentHouseDto;
import com.bem.estate.dto.listing.RentDetailsDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RentApartmentHouseDto {

    private RentDetailsDto rentDetails;

    private ApartmentHouseDto apartmentHouse;

}
