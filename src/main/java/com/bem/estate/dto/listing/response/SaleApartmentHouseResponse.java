package com.bem.estate.dto.listing.response;

import com.bem.estate.dto.listing.ApartmentHouseDto;
import com.bem.estate.dto.listing.SaleDetailsDto;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class SaleApartmentHouseResponse {

    private SaleDetailsDto saleDetails;
    private ApartmentHouseDto apartmentHouse;
}
