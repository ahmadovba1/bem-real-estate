package com.bem.estate.dto.listing;

import com.bem.estate.domain.listing.House;
import com.bem.estate.domain.listing.Land;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Setter
@Getter
public class IndependentHouseDto {

    private LandDto land;
    private HouseDto house;
}
