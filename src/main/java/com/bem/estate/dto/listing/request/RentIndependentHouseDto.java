package com.bem.estate.dto.listing.request;

import com.bem.estate.dto.listing.IndependentHouseDto;
import com.bem.estate.dto.listing.RentDetailsDto;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class RentIndependentHouseDto {


    private RentDetailsDto rentDetails;

    private IndependentHouseDto independentHouse;
}
