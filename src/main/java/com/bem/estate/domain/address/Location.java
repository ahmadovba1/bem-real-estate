package com.bem.estate.domain.address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    Long cityId;
    Long districtId;
    Long provinceId;
//    Long villageId;
    Double lat_t;
    Double long_t;
    String addressDescription;


//    @ManyToMany
//    List<NearPoint> nearPoints;

}

