package com.bem.estate.domain.listing;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Land  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private CommonListingDetails commonListingDetails;

    private Double landSquareOfficial;
    private Double landSquareUnOfficial;
}
