package com.bem.estate.domain.listing.sale.house;

import com.bem.estate.domain.listing.ApartmentHouse;
import com.bem.estate.domain.listing.SaleDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class SaleApartmentHouse  {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private SaleDetails saleDetails;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private ApartmentHouse apartmentHouse;
}
