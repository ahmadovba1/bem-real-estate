package com.bem.estate.domain.listing;

import com.bem.estate.domain.address.Location;
import com.bem.estate.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Version;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@Entity
public class CommonListingDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;


    private Double price;
    private Boolean officialRegistrationDocument;
    private Boolean workWithAgent;


    @Cascade(CascadeType.ALL)
    @OneToOne
    private Location location;

    private String listingDescription;

    @Enumerated(EnumType.STRING)
    private Status status;
}
