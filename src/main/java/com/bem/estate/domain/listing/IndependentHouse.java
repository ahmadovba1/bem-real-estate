package com.bem.estate.domain.listing;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@Entity
public class IndependentHouse  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private Land land;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private House house;
}
