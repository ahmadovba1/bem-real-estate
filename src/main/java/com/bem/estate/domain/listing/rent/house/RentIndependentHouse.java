package com.bem.estate.domain.listing.rent.house;

import com.bem.estate.domain.listing.ApartmentHouse;
import com.bem.estate.domain.listing.IndependentHouse;
import com.bem.estate.domain.listing.RentDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class RentIndependentHouse  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private RentDetails rentDetails;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private IndependentHouse independentHouse;
}
