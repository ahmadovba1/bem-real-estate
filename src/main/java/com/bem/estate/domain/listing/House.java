package com.bem.estate.domain.listing;

import com.bem.estate.enums.BuildingProject;
import com.bem.estate.enums.HouseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class House  {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Cascade(CascadeType.ALL)
    @OneToOne
    private Building building;

    @Enumerated(EnumType.STRING)
    private HouseType houseType;

}
