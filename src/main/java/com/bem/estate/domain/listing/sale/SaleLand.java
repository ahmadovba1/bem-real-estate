package com.bem.estate.domain.listing.sale;

import com.bem.estate.domain.listing.SaleDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.OneToOne;

//@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class SaleLand {

    @OneToOne
    private SaleDetails commonSaleDetails;
}
