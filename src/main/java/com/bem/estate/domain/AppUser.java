package com.bem.estate.domain;

import com.bem.estate.enums.AppUserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AppUser {
    Long id;

    String name;
    String surname;
    String sex;
    String email;
    String password;
    String fbLink;
    String instagramLink;
    String mobileNumber;

//    @ManyToMany
//    Set<Listing> favourities;

//    @Enumerated(EnumType.STRING)
//    AppUserType userType;
}
