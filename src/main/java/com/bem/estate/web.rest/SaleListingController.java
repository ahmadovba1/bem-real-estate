package com.bem.estate.web.rest;

import com.bem.estate.dto.listing.request.SaleApartmentHouseDto;
import com.bem.estate.dto.listing.request.SaleIndependentHouseDto;
import com.bem.estate.service.SaleListingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/listing/sale")
@Slf4j
public class SaleListingController {
    private final SaleListingService saleListingService;


    @PostMapping("/house/apartment")
    private ResponseEntity listApartmentHouseSale(
            @RequestBody SaleApartmentHouseDto saleApartmentHouseDto) {
        saleListingService.listSaleApartmentHouse(saleApartmentHouseDto);
        return ResponseEntity.status(HttpStatus.CREATED).body("sale apartment house created");
    }


    @PostMapping("/house/independent")
    private ResponseEntity listIndependentHouseSale(
            @RequestBody SaleIndependentHouseDto saleIndependentHouseDto) {
        saleListingService.listSaleIndependentHouse(saleIndependentHouseDto);
        return ResponseEntity.status(HttpStatus.CREATED).body("sale independent house created");

    }

    @GetMapping("/house/apartment")
    private List<SaleApartmentHouseDto> getAllApartmentHouseSale() {
        return saleListingService.findAllSaleApartmentHouse();
    }

    @GetMapping("/house/independent")
    private List<SaleIndependentHouseDto> getAllIndependentHouseSale() {
        return saleListingService.findAllSaleIndependentHouse();
    }
}