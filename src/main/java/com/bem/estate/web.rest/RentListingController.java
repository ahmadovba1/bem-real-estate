package com.bem.estate.web.rest;

import com.bem.estate.dto.listing.request.RentApartmentHouseDto;
import com.bem.estate.dto.listing.request.RentIndependentHouseDto;
import com.bem.estate.dto.listing.request.SaleApartmentHouseDto;
import com.bem.estate.dto.listing.request.SaleIndependentHouseDto;
import com.bem.estate.service.RentListingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/listing/rent")
@Slf4j
public class RentListingController {
    private final RentListingService rentListingService;

    @PostMapping("/rent/house/apartment")
    private ResponseEntity<String> listApartmentHouseRent(
            @RequestBody RentApartmentHouseDto rentApartmentHouse) {
        rentListingService.listRentApartmentHouse(rentApartmentHouse);
        return ResponseEntity.status(HttpStatus.CREATED).body("rent apartment house created");

    }

    @PostMapping("/rent/house/independent")
    private ResponseEntity<String> listIndependentHouseRent(
            @RequestBody RentIndependentHouseDto rentIndependentHouseDto) {
        rentListingService.listRentIndependentHouse(rentIndependentHouseDto);
        return ResponseEntity.status(HttpStatus.CREATED).body("rent independent house created");

    }

    @GetMapping("/house/apartment")
    private List<RentApartmentHouseDto> getAllApartmentHouseSale() {
        return rentListingService.findAllRentApartmentHouse();
    }

    @GetMapping("/house/independent")
    private List<RentIndependentHouseDto> getAllIndependentHouseSale() {
        return rentListingService.findAllRentIndependentHouse();
    }
}